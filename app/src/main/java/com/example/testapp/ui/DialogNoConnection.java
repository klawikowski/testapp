package com.example.testapp.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.testapp.R;

/**
 * Dialog with no connection info
 */
public class DialogNoConnection
{
    public static void DialogNoConnection(Context pContext, DialogInterface.OnClickListener pAccept, DialogInterface.OnClickListener pDecline)
    {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(pContext);
        builder.setTitle(R.string.dialog_no_connection_title);
        builder.setMessage(R.string.dialog_no_connection_body);
        if(pAccept != null)
        {
            builder.setPositiveButton(android.R.string.ok, pAccept);
        }
        if(pDecline != null)
        {
            builder.setNegativeButton(android.R.string.cancel, pDecline);
        }
        builder.show();
    }

}
