package com.example.testapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import com.example.testapp.R;
import com.example.testapp.activity.ActivityDetails;
import com.example.testapp.fragment.adapter.AdapterMainList;
import com.example.testapp.fragment.adapter.IDataProvider;
import com.example.testapp.loaders.LoaderEntity;
import com.example.testapp.model.Entity;

import java.util.ArrayList;

/**
 * List Fragment for {@link com.example.testapp.activity.ActivityMain}
 */
public class FragmentMainList extends FragmentBase implements AdapterMainList.ListClickListener, IDataProvider<Entity>, LoaderManager.LoaderCallbacks<ArrayList<Entity>>
{

    private RecyclerView mRecyclerView;
    private ArrayList<Entity> mEntities = new ArrayList<>();
    private AdapterMainList mAdapterMainList;

    @Override
    public int getLayoutId()
    {
        return R.layout.fragment_main_list;
    }

    @Override
    public void setupLogic(View pFragmentView)
    {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapterMainList = new AdapterMainList(this, this);
        mRecyclerView.setAdapter(mAdapterMainList);
        getLoaderManager().initLoader(mRecyclerView.getId(), null, this).forceLoad();
    }

    @Override
    public void setupComponents(View pFragmentView)
    {
        mRecyclerView = (RecyclerView) pFragmentView.findViewById(R.id.fragment_main_list_recycler_view);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        FragmentDetails displayFrag = (FragmentDetails) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.activity_main_fragment_details);

        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(displayFrag != null && (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270))
        {
            mAdapterMainList.setSelectionEnabled(true);
            mAdapterMainList.notifyDataSetChanged();
        }
        else
        {
            mAdapterMainList.setSelectionEnabled(false);
            mAdapterMainList.notifyDataSetChanged();
        }
    }

    @Override
    public void onListItemClick(int pPosition, Entity pEntity, View pView)
    {
        mAdapterMainList.setSelectedCellIndex(pPosition);

        //https://developer.android.com/guide/practices/tablets-and-handsets.html
        FragmentDetails displayFrag = (FragmentDetails) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.activity_main_fragment_details);

        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        if(displayFrag != null && (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270))
        {
            mAdapterMainList.setSelectionEnabled(true);
            mAdapterMainList.notifyDataSetChanged();
            displayFrag.loadNewData(pEntity.getName());
        }
        else
        {
            mAdapterMainList.setSelectionEnabled(false);
            mAdapterMainList.notifyDataSetChanged();
            Intent detailIntent = new Intent(getContext(), ActivityDetails.class);
            detailIntent.putExtra(ActivityDetails.BUNDLE_ENTITY_NAME, pEntity.getName());
            startActivity(detailIntent);
        }
    }

    @Override
    public ArrayList<Entity> getData()
    {
        return mEntities;
    }


    @Override
    public Loader<ArrayList<Entity>> onCreateLoader(int id, Bundle args)
    {
        return new LoaderEntity(getContext());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Entity>> loader, ArrayList<Entity> data)
    {
        mEntities.clear();
        mEntities.addAll(data);
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Entity>> loader)
    {
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }
}
