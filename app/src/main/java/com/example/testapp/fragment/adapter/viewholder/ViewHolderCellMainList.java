package com.example.testapp.fragment.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testapp.R;

/**
 * View Holder for Cell Main List
 */
public class ViewHolderCellMainList extends RecyclerView.ViewHolder
{
    private TextView mTitle;
    private ImageView mImage;
    private View mCellView;

    public ViewHolderCellMainList(View itemView)
    {
        super(itemView);
        mTitle = (TextView) itemView.findViewById(R.id.cell_main_list_title);
        mImage = (ImageView) itemView.findViewById(R.id.cell_main_list_image);
        mCellView = itemView;
    }

    public View getCellView()
    {
        return mCellView;
    }

    public TextView getTitle()
    {
        return mTitle;
    }

    public ImageView getImage()
    {
        return mImage;
    }
}
