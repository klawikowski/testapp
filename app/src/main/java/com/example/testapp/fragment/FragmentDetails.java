package com.example.testapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.testapp.R;
import com.example.testapp.loaders.LoaderEntityDetails;
import com.example.testapp.model.EntityDetails;
import com.squareup.picasso.Picasso;

/**
 * Fragment entity details
 */
public class FragmentDetails extends FragmentBase implements LoaderManager.LoaderCallbacks<EntityDetails>
{
    public static String SAVE_INSTANCE_ENTITY_INDEX = "SAVE_INSTANCE_ENTITY_INDEX";

    private String mEntityIndex = "1";

    private TextView mTitle;
    private ImageView mImageView;
    private LoaderEntityDetails mLoaderEntityDetails;

    public static FragmentDetails newInstance(String pEntityIndex)
    {
        FragmentDetails fragment = new FragmentDetails();
        fragment.mEntityIndex = pEntityIndex;
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(SAVE_INSTANCE_ENTITY_INDEX, mEntityIndex);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null)
        {
            mEntityIndex = savedInstanceState.getString(SAVE_INSTANCE_ENTITY_INDEX);
        }
    }

    @Override
    public int getLayoutId()
    {
        return R.layout.layout_fragment_details;
    }

    @Override
    public void setupLogic(View pFragmentView)
    {
        getLoaderManager().initLoader(0, null, this).forceLoad();
    }

    @Override
    public void setupComponents(View pFragmentView)
    {
        mTitle = (TextView) pFragmentView.findViewById(R.id.fragment_details_title);
        mImageView = (ImageView) pFragmentView.findViewById(R.id.fragment_details_image);
        mLoaderEntityDetails = new LoaderEntityDetails(getContext(), mEntityIndex);
    }

    @Override
    public Loader<EntityDetails> onCreateLoader(int id, Bundle args)
    {
        return mLoaderEntityDetails;
    }

    @Override
    public void onLoadFinished(Loader<EntityDetails> loader, EntityDetails data)
    {
        refreshUI(data);
    }

    @Override
    public void onLoaderReset(Loader<EntityDetails> loader)
    {

    }

    /**
     * Loads new data to fragment
     *
     * @param pEntityIndex
     */
    public void loadNewData(String pEntityIndex)
    {
        mEntityIndex = pEntityIndex;
        mLoaderEntityDetails = new LoaderEntityDetails(getContext(), pEntityIndex);
        getLoaderManager().destroyLoader(0);
        getLoaderManager().initLoader(0, null, this).forceLoad();
    }


    private void refreshUI(EntityDetails pEntityDetails)
    {
        if(pEntityDetails != null)
        {
            mTitle.setText(pEntityDetails.getText());
            Picasso.with(mImageView.getContext()).load(pEntityDetails.getImage()).into(mImageView);
        }
    }
}
