package com.example.testapp.fragment.adapter;

import java.util.ArrayList;

/**
 * Generic Data Provider for adapter
 */
public interface IDataProvider<T>
{
    /**
     * @return Data to display
     */
    ArrayList<T> getData();
}
