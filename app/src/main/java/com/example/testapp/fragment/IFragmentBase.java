package com.example.testapp.fragment;

import android.view.View;

/**
 * Adapter interface for {@link FragmentBase}
 */
public interface IFragmentBase
{
    /**
     * Method should return R.layout reference to layout which should be used as fragment view
     *
     * @return R.layout reference to layout
     */
    int getLayoutId();

    /**
     * Place to implement all logic
     *
     * @param pFragmentView
     */
    void setupLogic(View pFragmentView);

    /**
     * Place to initialize components
     *
     * @param pFragmentView
     */
    void setupComponents(View pFragmentView);
}
