package com.example.testapp.fragment.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testapp.R;
import com.example.testapp.fragment.adapter.viewholder.ViewHolderCellMainList;
import com.example.testapp.model.Entity;
import com.squareup.picasso.Picasso;

/**
 * Recycler View Adapter for {@link com.example.testapp.fragment.FragmentMainList}
 */
public class AdapterMainList extends RecyclerView.Adapter<ViewHolderCellMainList>
{
    private ListClickListener mListClickListener;
    private IDataProvider<Entity> mDataProvider;
    private int mSelectedCell = 0;
    private boolean isSelectionEnabled = true;

    public AdapterMainList(@Nullable ListClickListener pListClickListener, IDataProvider<Entity> pDataProvider)
    {
        mListClickListener = pListClickListener;
        mDataProvider = pDataProvider;
    }

    public int getSelectedCellIndex()
    {
        return mSelectedCell;
    }

    public void setSelectedCellIndex(int pSelectedCell)
    {
        mSelectedCell = pSelectedCell;
    }

    public boolean isSelectionEnabled()
    {
        return isSelectionEnabled;
    }

    public void setSelectionEnabled(boolean pSelectionEnabled)
    {
        isSelectionEnabled = pSelectionEnabled;
    }

    @Override
    public ViewHolderCellMainList onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View cellView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_main_list, parent, false);
        ViewHolderCellMainList viewHolder = new ViewHolderCellMainList(cellView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderCellMainList holder, final int position)
    {
        if(mDataProvider != null && mDataProvider.getData() != null)
        {
            final Entity entity = mDataProvider.getData().get(position);
            if(entity != null)
            {
                holder.getTitle().setText(mDataProvider.getData().get(position).getName());
                Picasso.with(holder.getTitle().getContext()).load(mDataProvider.getData().get(position).getImageUrl()).into(holder.getImage());
                holder.getCellView().setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(mListClickListener != null)
                        {
                            mListClickListener.onListItemClick(position, entity, v);
                        }
                    }
                });

                if(isSelectionEnabled && position == getSelectedCellIndex())
                {
                    holder.getCellView().setSelected(true);
                }
                else
                {
                    holder.getCellView().setSelected(false);
                }
            }
        }
    }

    @Override
    public int getItemCount()
    {
        if(mDataProvider != null && mDataProvider.getData() != null)
        {
            return mDataProvider.getData().size();
        }
        return 0;
    }

    /**
     * Listener for list clicks
     */
    public interface ListClickListener
    {
        void onListItemClick(int pPosition, Entity pEntity, View pView);
    }
}
