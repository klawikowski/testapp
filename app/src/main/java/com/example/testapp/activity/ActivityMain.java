package com.example.testapp.activity;

import android.support.v7.widget.Toolbar;

import com.example.testapp.R;

/**
 * Activity Main class
 */
public class ActivityMain extends ActivityBase
{
    private Toolbar mToolbar;

    @Override
    public int getActivityLayoutId()
    {
        return R.layout.layout_activity_main;
    }

    @Override
    public void setupComponents()
    {
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
    }

    @Override
    public void setupLogic()
    {
        setupToolbar();
    }

    /**
     * Toolbar logic / actions setup
     */
    private void setupToolbar()
    {
        //simple check -lets be sure we're safe
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }
    }
}
