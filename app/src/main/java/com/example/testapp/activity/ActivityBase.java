package com.example.testapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Base Activity handling all init / setup logic
 * Aswell some common logic might be implemented here
 */
public abstract class ActivityBase extends AppCompatActivity implements IActivityBase
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayoutId());
        getIntentData();
        setupComponents();
        setupLogic();
    }

    //implemented because we dont want to use it everywhere
    @Override
    public void getIntentData()
    {

    }
}
