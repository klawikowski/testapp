package com.example.testapp.activity;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.testapp.R;
import com.example.testapp.fragment.FragmentDetails;

/**
 * Details activity
 */
public class ActivityDetails extends ActivityBase
{
    public static final String BUNDLE_ENTITY_NAME = "BUNDLE_DETAIL_ENTITY_NAME";
    private Toolbar mToolbar;
    private String mEntityIndex = "0";

    @Override
    public int getActivityLayoutId()
    {
        return R.layout.layout_activity_details;
    }

    @Override
    public void setupComponents()
    {
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
    }

    @Override
    public void setupLogic()
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(mEntityIndex);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        getSupportFragmentManager().beginTransaction().add(R.id.activity_details_container, FragmentDetails.newInstance(mEntityIndex)).commit();
    }

    @Override
    public void getIntentData()
    {
        if(getIntent() != null)
        {
            mEntityIndex = getIntent().getExtras().getString(BUNDLE_ENTITY_NAME);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
