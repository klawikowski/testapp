package com.example.testapp.activity;

/**
 * Interface for {@link ActivityBase}
 */
public interface IActivityBase
{
    /**
     * Gets value of R.layout id which will be used as main layout for activity
     * @return R.layout id reference to layout
     */
    int getActivityLayoutId();

    /**
     * Place to setup references to layout components
     */
    void setupComponents();

    /**
     * Place to setup logic for components
     */
    void setupLogic();

    /**
     * Place to get intent data
     */
    void getIntentData();
}
