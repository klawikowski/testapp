package com.example.testapp.model;

import org.json.JSONObject;

/**
 * POJO for single entity
 */
public class Entity
{
    private String mName;
    private String mImageUrl;

    public Entity(String pName, String pImageUrl)
    {
        mName = pName;
        mImageUrl = pImageUrl;
    }

    /**
     * Constructor from JSONObject
     *
     * @param pJSONObject
     * @return True if json was valid. false if not
     */
    public Entity(JSONObject pJSONObject) throws Exception
    {
        mName = pJSONObject.getString("name");
        mImageUrl = pJSONObject.getString("image");
    }

    public String getName()
    {
        return mName;
    }

    public void setName(String pName)
    {
        mName = pName;
    }

    public String getImageUrl()
    {
        return mImageUrl;
    }

    public void setImageUrl(String pImageUrl)
    {
        mImageUrl = pImageUrl;
    }
}
