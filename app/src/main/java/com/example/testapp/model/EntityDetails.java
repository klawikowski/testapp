package com.example.testapp.model;

import org.json.JSONObject;

/**
 * POJO for Entity Details
 */
public class EntityDetails
{
    private String mName;
    private String mText;
    private String mImage;

    public EntityDetails(JSONObject pJSONObject) throws Exception
    {
        mName = pJSONObject.getString("name");
        mText = pJSONObject.getString("text");
        mImage = pJSONObject.getString("image");
    }

    public EntityDetails(String pName, String pText, String pImagel)
    {
        mName = pName;
        mText = pText;
        mImage = pImagel;
    }

    public String getName()
    {
        return mName;
    }

    public void setName(String pName)
    {
        mName = pName;
    }

    public String getText()
    {
        return mText;
    }

    public void setText(String pText)
    {
        mText = pText;
    }

    public String getImage()
    {
        return mImage;
    }

    public void setImage(String pImage)
    {
        mImage = pImage;
    }
}
