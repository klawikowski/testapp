package com.example.testapp.loaders;

import android.content.Context;

import com.example.testapp.model.EntityDetails;

import org.json.JSONObject;

/**
 * Entity loader
 */
public class LoaderEntityDetails extends LoaderBase<EntityDetails>
{
    public static final String WS_ENTITY_DETAIL_URL = "http://dev.tapptic.com/test/json.php?name=";


    public LoaderEntityDetails(Context context, String pParamIndex)
    {
        super(context, WS_ENTITY_DETAIL_URL + pParamIndex);
    }

    @Override
    EntityDetails parseResult(String pRawResponse)
    {
        try
        {
            JSONObject jsonResponse = new JSONObject(pRawResponse);
            //looper try
            EntityDetails entityDetails = new EntityDetails(jsonResponse);
            return entityDetails;
        }
        catch(Exception e)
        {
            return null;
        }
    }
}
