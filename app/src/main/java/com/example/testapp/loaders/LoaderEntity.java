package com.example.testapp.loaders;

import android.content.Context;

import com.example.testapp.model.Entity;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Entity Loader
 */
public class LoaderEntity extends LoaderBase<ArrayList<Entity>>
{
    public static final String WS_ENTITY_LIST_URL = "http://dev.tapptic.com/test/json.php";

    public LoaderEntity(Context context)
    {
        super(context, WS_ENTITY_LIST_URL);
    }

    @Override
    ArrayList<Entity> parseResult(String pRawResponse)
    {
        ArrayList<Entity> ret = new ArrayList<>();
        try
        {
            JSONArray array = new JSONArray(pRawResponse);
            for(int i = 0; i < array.length(); i++)
            {
                //looper try
                try
                {
                    Entity entity = new Entity(array.getJSONObject(i));
                    ret.add(entity);
                }
                catch(Exception singleEntityAddException)
                {

                }
            }
        }
        catch(Exception e)
        {
            //malformed json
        }
        return ret;
    }
}
