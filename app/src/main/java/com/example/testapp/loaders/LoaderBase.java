package com.example.testapp.loaders;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.AsyncTaskLoader;

import com.example.testapp.helper.HelperConnection;
import com.example.testapp.ui.DialogNoConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Abstract Loader
 */
public abstract class LoaderBase<T> extends AsyncTaskLoader<T>
{
    protected String mUrl;
    protected Context mContext;
    protected LoaderBase<T> mInstance;

    public LoaderBase(Context context, String pEndpointUrl)
    {
        super(context);
        mContext = context;
        mUrl = pEndpointUrl;
        mInstance = this;
    }

    @Override
    protected void onStartLoading()
    {
        if(HelperConnection.isOnline(mContext))
        {
            super.onStartLoading();
        }
        else
        {
            mInstance.stopLoading();
            DialogNoConnection.DialogNoConnection(mContext, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    if(HelperConnection.isOnline(mContext))
                    {
                        mInstance.forceLoad();
                    }
                }
            }, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mInstance.stopLoading();
                }
            });
        }
    }

    @Override
    public T loadInBackground()
    {
        try
        {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(mUrl)
                    .build();

            Response response = client.newCall(request).execute();


            String rawResponse = response.body().string();


            return parseResult(rawResponse);
        }
        catch(Exception e)
        {

        }
        return parseResult(null);
    }

    /**
     * Place to parse result
     *
     * @param pRawResponse json string
     * @return Generic return type
     */
    abstract T parseResult(String pRawResponse);
}
